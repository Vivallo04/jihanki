package com.lambda.jihanki;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.lambda.jihanki.screens.MainGameScreen;

public class Jihanki extends Game {

	private static final String TAG = Jihanki.class.getSimpleName();

	private static MainGameScreen gameScreen;
	public SpriteBatch batch;

	private static enum ScreenType {
		MainMenu,
		MainGame,
		LoadGame,
		NewGame,
		GameOver,
		Credits
	}

	public Screen getScreenType(ScreenType screenType) {
		switch (screenType) {
			case MainMenu:
				return gameScreen;
			case MainGame:
				return gameScreen;
			case LoadGame:
				return gameScreen;
			case NewGame:
				return gameScreen;
			case GameOver:
				return gameScreen;
			case Credits:
				return gameScreen;
			default:
				return gameScreen;
		}
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		gameScreen = new MainGameScreen(this);
		setScreen(gameScreen);
	}

	@Override
	public void render() {
		super.render();

	}

	@Override
	public void dispose () {
		gameScreen.dispose();
	}

}
