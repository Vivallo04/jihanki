package com.lambda.jihanki.audio;

public interface AudioSubject {
    void addObserver(AudioObserver audioObserver);
    void removeObserver(AudioObserver audioObserver);
    void removeAllObservers();
}
