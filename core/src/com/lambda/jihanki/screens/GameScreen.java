package com.lambda.jihanki.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.Array;
import com.lambda.jihanki.audio.AudioManager;
import com.lambda.jihanki.audio.AudioObserver;
import com.lambda.jihanki.audio.AudioSubject;

public class GameScreen implements Screen, AudioSubject {

    private Array<AudioObserver> observers;

    public GameScreen() {
        this.observers = new Array<AudioObserver>();
        //this.addObserver(AudioManager.getInstance());
    }

    /**
     * Called when this screen becomes the current screen for a {@link com.lambda.jihanki.Jihanki}.
     */
    @Override
    public void show() {

    }

    /**
     * Called when the screen should render itself.
     *
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {

    }

    /**
     * @param width
     * @param height
     */
    @Override
    public void resize(int width, int height) {

    }


    @Override
    public void pause() {

    }


    @Override
    public void resume() {

    }

    /**
     * Called when this screen is no longer the current screen for a {@link com.lambda.jihanki.Jihanki}.
     */
    @Override
    public void hide() {

    }

    /**
     * Called when this screen should release all resources.
     */
    @Override
    public void dispose() {

    }

    @Override
    public void addObserver(AudioObserver audioObserver) {
        observers.add(audioObserver);
    }

    @Override
    public void removeObserver(AudioObserver audioObserver) {
        observers.removeValue(audioObserver, true);
    }

    @Override
    public void removeAllObservers() {
        observers.removeAll(observers, true);
    }
}
