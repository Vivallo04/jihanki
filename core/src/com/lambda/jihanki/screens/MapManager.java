package com.lambda.jihanki.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.lambda.jihanki.util.Utility;

public class MapManager {

    private static final String TAG = MapManager.class.getSimpleName();
    private static final String defaultTilesPath = "graphics/IsometricFantasyTiles.png";

    private Array<TextureRegion> grassFrames;
    protected TextureRegion currentFrame;
    protected Sprite frameSprite;

    public final int TILE_WIDTH = 16;
    public final int TILE_HEIGHT = 16;

    private final Texture grass;

    public MapManager() {
        grass = new Texture(Gdx.files.internal("graphics/grass.png"));

        Utility.loadTextureAsset(defaultTilesPath);
        loadDefaultSprite();
    }

    private void loadDefaultSprite() {
        Texture texture = Utility.getTextureAsset(defaultTilesPath);
        TextureRegion[][] textureFrames = TextureRegion.split(texture, TILE_WIDTH, TILE_HEIGHT);

        frameSprite = new Sprite(textureFrames[0][0].getTexture(), 0, 0, TILE_WIDTH, TILE_HEIGHT);
        currentFrame = textureFrames[0][0];
        Gdx.app.debug(TAG, "Sprites loaded, total Tiles: " + textureFrames.length);
    }

    private TextureRegion getFrameSprite() {
        return currentFrame;
    }

    public void drawGround(SpriteBatch batch) {
        for (int i = 20; i >= 0; i--) {
            for (int j = 20; j >= 0 ; j--) {
                float x = (j - i) * (TILE_WIDTH  / 2f);
                float y = (j + i) * (TILE_HEIGHT / 4f);
                batch.draw(getFrameSprite(), x, y, TILE_WIDTH, TILE_HEIGHT);
            }
        }
    }

}
