package com.lambda.jihanki.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.Json;
import com.lambda.jihanki.Jihanki;
import com.lambda.jihanki.entity.Entity;
import com.lambda.jihanki.entity.PlayerController;
import com.lambda.jihanki.entity.PlayerHUD;

public class MainGameScreen implements Screen {

    private static final String TAG = MainGameScreen.class.getSimpleName();

    public static class VIEWPORT {
        static float viewportWidth;
        static float viewportHeight;
        static float virtualWidth;
        static float virtualHeight;
        static float physicalWidth;
        static float physicalHeight;
        static float aspectRatio;
    }

    public enum GameState {
        SAVING,
        LOADING,
        RUNNING,
        PAUSED,
        GAME_OVER
    }

    private PlayerController controller;
    private TextureRegion currentPlayerFrame;
    private Sprite currentPlayerSprite;

    private OrthogonalTiledMapRenderer mapRenderer = null;
    private OrthographicCamera hudCamera;
    private OrthographicCamera camera;
    private MapManager mapManager;
    private GameState gameState;

    private Jihanki game;
    private Json json;

    private Entity player;
    private PlayerHUD playerHUD;

    public MainGameScreen(Jihanki game) {
        this.game = game;
        this.json = new Json();
        this.mapManager = new MapManager();

        setGameState(GameState.RUNNING);

        Gdx.app.debug(TAG, "Current game state is: " + getGameState());
    }


    /**
     * Called when this screen becomes the current screen for a {@link Jihanki}.
     */
    @Override
    public void show() {
        // Camera setup
        setupViewport(10, 10);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, VIEWPORT.viewportWidth, VIEWPORT.viewportHeight);

        //mapRenderer = new OrthogonalTiledMapRenderer();
        mapRenderer.setView(camera);
        player = new Entity();
        //player.init();
        currentPlayerSprite = player.getFrameSprite();
        controller = new PlayerController(player);
        Gdx.input.setInputProcessor(controller);
    }

    /**
     * Called when the screen should render itself.
     * @param delta The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255, 255, 255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.position.set(currentPlayerSprite.getX(), currentPlayerSprite.getY(), 0f);
        camera.update();
        player.update(delta);
        currentPlayerFrame = player.getFrame();

        controller.update(delta);



        game.batch.begin();
        if (this.gameState == GameState.RUNNING) {
            mapManager.drawGround(game.batch);
        }
        game.batch.end();
    }

    public void setGameState(GameState state) {
        switch (state) {
            case RUNNING:
                this.gameState = GameState.RUNNING;
                break;
            case LOADING:
                this.gameState = GameState.LOADING;
                break;
            case SAVING:
                this.gameState = GameState.PAUSED;
                break;
            case PAUSED:
                if (this.gameState == GameState.PAUSED) {
                    this.gameState = GameState.RUNNING;

                } else if (this.gameState == GameState.RUNNING) {
                    this.gameState = GameState.PAUSED;
                }
                break;

            case GAME_OVER:
                this.gameState = GameState.GAME_OVER;
                break;
            default:
                this.gameState = GameState.RUNNING;
                break;
        }
    }

    public GameState getGameState() {
        return this.gameState;
    }

    /**
     * @param width new Window width
     * @param height new Window height
     */
    @Override
    public void resize(int width, int height) {
        setupViewport(10, 10);
        camera.setToOrtho(false, VIEWPORT.viewportWidth, VIEWPORT.viewportHeight);
    }


    @Override
    public void pause() {
        setGameState(GameState.SAVING);
    }


    @Override
    public void resume() {
        setGameState(GameState.LOADING);
    }

    /**
     * Called when this screen is no longer the current screen for a {@link Jihanki}.
     */
    @Override
    public void hide() {

    }

    /**
     * Called when this screen should release all resources.
     */
    @Override
    public void dispose() {
        player.dispose();
       //controller.dispose();
        Gdx.input.setInputProcessor(null);
        //.dispose();
    }

    private void setupViewport(int width, int height) {
        // Make the viewport a percentage of the total display area
        VIEWPORT.virtualWidth = width;
        VIEWPORT.virtualHeight = height;

        // Current viewport dimensions
        VIEWPORT.viewportWidth = VIEWPORT.virtualWidth;
        VIEWPORT.viewportHeight = VIEWPORT.virtualHeight;

        // Pixel dimensions of display
        VIEWPORT.physicalWidth = Gdx.graphics.getWidth();
        VIEWPORT.physicalHeight = Gdx.graphics.getHeight();

        // Aspect ratio for the current viewport
        VIEWPORT.aspectRatio = (VIEWPORT.virtualWidth / VIEWPORT.virtualHeight);

        // Update the viewport if there could be skewing
        if (VIEWPORT.physicalWidth / VIEWPORT.physicalHeight >= VIEWPORT.aspectRatio) {

            // Letterbox left and right
            VIEWPORT.viewportWidth = VIEWPORT.viewportHeight *
                    (VIEWPORT.physicalWidth / VIEWPORT.physicalHeight);
            VIEWPORT.viewportHeight = VIEWPORT.virtualHeight;
        } else {

            // Letterbox above and below
            VIEWPORT.viewportWidth  = VIEWPORT.virtualWidth;
            VIEWPORT.viewportHeight = VIEWPORT.viewportWidth *
                    (VIEWPORT.physicalHeight / VIEWPORT.physicalWidth);
        }

        Gdx.app.debug(TAG, "WorldRender: virtual (" + VIEWPORT.virtualWidth +
                ","  + VIEWPORT.virtualHeight + ")");

        Gdx.app.debug(TAG, "WorldRender: VIEWPORT (" + VIEWPORT.viewportWidth +
                ","  + VIEWPORT.viewportHeight + ")");

        Gdx.app.debug(TAG, "WorldRender: physical (" + VIEWPORT.physicalWidth +
                ","  + VIEWPORT.physicalHeight + ")");
    }
}
