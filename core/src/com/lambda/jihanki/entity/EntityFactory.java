package com.lambda.jihanki.entity;

import com.badlogic.gdx.utils.Json;

import java.util.Hashtable;

public class EntityFactory {

    private final String TAG = EntityFactory.class.getSimpleName();

    private static final Json json = new Json();
    private static final EntityFactory instance = null;
    private Hashtable<String, Entity> entities;

    public enum EntityType {
        PLAYER,
        NPC
    }

    public enum EntityName {

    }
}
