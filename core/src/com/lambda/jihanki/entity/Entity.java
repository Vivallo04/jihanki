package com.lambda.jihanki.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.lambda.jihanki.util.Utility;

import java.util.UUID;


public class Entity {

    /*
      The TextureRegion class is analogous to using a sprite
      (a Texture region) from a sprite sheet(Texture).
      When we load an image asset, we will be sorting the image
      asset as a Texture object in the AssetManager class by using
      the loadTextureAsset() and getTextureAssetMethod() methods
      from the Utility class.

      In this case, the Texture object will include the entire
      image consisting of 16 different sprites. In order to reference
      a specific sprite, TextureRegion can be used to get access to a
      subregion of the Texture object.
     */

    private static final String TAG = Entity.class.getSimpleName();
    private static final String defaultSpritePath = "graphics/EntitiesOutlined.png";

    private Vector2 velocity;
    private String entityID;

    private Direction currentDirection = Direction.LEFT;
    private Direction previousDirection = Direction.UP;

    private Animation walkLeftAnimation;
    private Animation walkRightAnimation;
    private Animation walkUpAnimation;
    private Animation walkDownAnimation;

    private Array<TextureRegion> walkLeftFrames;
    private Array<TextureRegion> walkRightFrames;
    private Array<TextureRegion> walUpFrames;
    private Array<TextureRegion> walkDownFrames;

    protected Vector2 nextPlayerPosition;
    protected Vector2 currentPlayerPosition;
    protected State state = State.IDLE;
    protected float frameTime = 0f;
    protected Sprite frameSprite = null;
    protected TextureRegion currentFrame = null;

    // Size of sprite on the sprite sheet
    public final int FRAME_WIDTH = 16;
    public final int FRAME_HEIGHT = 16;
    public static Rectangle boundingBox;

    public enum State {
        IDLE,
        WALKING,
        JUMPING,
        IMMOBILE;

        static public State getRandomNext() {
            return State.values()[MathUtils.random(State.values().length - 2)];
        }
    }

    public enum Direction {
        UP,
        RIGHT,
        DOWN,
        LEFT;

        static public Direction getRandomNext() {
            return Direction.values()[MathUtils.random(Direction.values().length - 1)];
        }

        public Direction getOpposite() {
            if (this == LEFT) {
                return RIGHT;
            } else if (this == RIGHT) {
                return LEFT;
            } else if (this == UP) {
                return DOWN;
            } else {
                return UP;
            }
        }
    }

    public Entity() {
        initEntity();
    }

    public void initEntity() {
        this.entityID = UUID.randomUUID().toString();
        this.nextPlayerPosition = new Vector2();
        this.currentPlayerPosition = new Vector2();
        boundingBox = new Rectangle();
        this.velocity = new Vector2(2f, 2f);

        Utility.loadTextureAsset(defaultSpritePath);
        loadDefaultSprite();
        loadAllAnimations();
    }

    /**
     * Called on any game object entity before it is rendered.
     * Note that depending on how long the game is playing, we
     * don't want to have a value increasing for the entire lifetime
     * of the game since there is a potential for an overflow, that's
     * why we mod the value to 5(reset the value every  seconds)
     * @param delta Time between last drawn frame and current frame
     */
    public void update(float delta) {
        // We want to avoid overflow so:
        frameTime = (frameTime + delta) % 5;

        // We want the hitbox to be at the feet
        // for a better feel
        setBoundingBoxSize(0f, 0.5f);
    }

    public void init(float startX, float startY) {
        this.currentPlayerPosition.x = startX;
        this.currentPlayerPosition.y = startY;

        this.nextPlayerPosition.x = startX;
        this.nextPlayerPosition.y = startY;
    }


    /**
     * This method allows us to customize the bounding box
     * for different entities, in this case we are using it
     * @param percentageWidthReduced
     * @param percentageHeightReduced
     */
    public void setBoundingBoxSize(float percentageWidthReduced, float percentageHeightReduced) {

        // Update the current bounding box;
        float width;
        float height;

        float widthReductionAmount = 1.0f - percentageWidthReduced;
        float heightReductionAmount  = 1.0f - percentageHeightReduced;

        if (widthReductionAmount > 0 && widthReductionAmount < 1) {
            width = FRAME_WIDTH * widthReductionAmount;
        } else {
            width = FRAME_WIDTH;
        }

        if (heightReductionAmount > 0 && heightReductionAmount < 1) {
            height = FRAME_HEIGHT * heightReductionAmount;
        } else {
            height = FRAME_HEIGHT;
        }

        if (width == 0 || height == 0) {
            Gdx.app.debug(TAG, "Width and height are zero." + width + ":" + "height");
        }

        // Need to account for the unit scale, since the
        // map coordinates will be in pixels
        float minX;
        float minY;
        /*if (MapManager.UNIT_SCALE > 0) {
            minX = nextPlayerPosition.x / MapManager.UNIT_SCALE;
            minY = nextPlayerPosition.y / MapManager.UNIT_SCALE;
        } else {
            minX = nextPlayerPosition.x;
            minY = nextPlayerPosition.y;
        }*/
       //boundingBox.set(minX, minY, width, height);
    }

    private void loadDefaultSprite() {
        Texture texture = Utility.getTextureAsset(defaultSpritePath);
        TextureRegion[][] textureFrames = TextureRegion.split(texture, FRAME_WIDTH, FRAME_HEIGHT);

        frameSprite = new Sprite(textureFrames[0][0].getTexture(), 0, 0, FRAME_WIDTH, FRAME_HEIGHT);
        currentFrame = textureFrames[0][0];
    }

    private void loadAllAnimations() {
        //Walking animation
        Texture texture = Utility.getTextureAsset(defaultSpritePath);
        TextureRegion[][] textureFrames = TextureRegion.split(texture, FRAME_WIDTH, FRAME_HEIGHT);

        walkDownFrames  = new Array<TextureRegion>(4);
        walkLeftFrames  = new Array<TextureRegion>(4);
        walkRightFrames = new Array<TextureRegion>(4);
        walkDownFrames  = new Array<TextureRegion>(4);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                TextureRegion region = textureFrames[i][j];

                if (region == null) {
                    Gdx.app.debug(TAG, "Got null animation frames " + i + ", " + j);
                }

                switch (i) {
                    case 0:
                        walkDownFrames.insert(j, region);
                        break;
                    case 1:
                        walkLeftFrames.insert(j, region);
                        break;
                    case 2:
                        walkRightFrames.insert(j, region);
                        break;
                    case 3:
                        walUpFrames.insert(j, region);
                        break;
                }
            }
        }
        walkDownAnimation  = new Animation(0.25f, walkDownFrames, Animation.PlayMode.LOOP);
        walkUpAnimation    = new Animation(0.25f, walUpFrames, Animation.PlayMode.LOOP);
        walkRightAnimation = new Animation(0.25f, walkRightFrames, Animation.PlayMode.LOOP);
        walkLeftAnimation  = new Animation(0.25f, walkLeftFrames, Animation.PlayMode.LOOP);
    }

    public void dispose() {
        Utility.unloadAsset(defaultSpritePath);
    }

    public void setState(State state) {
        this.state = state;
    }

    public Sprite getFrameSprite() {
        return frameSprite;
    }
    //**
    public TextureRegion getFrame() {
        return currentFrame;
    }

    public Vector2 getCurrentPlayerPosition() {
        return currentPlayerPosition;
    }

    public void setCurrentPosition(float currentPositionX, float currenPositionY) {
        frameSprite.setX(currentPositionX);
        frameSprite.setY(currenPositionY);

        this.currentPlayerPosition.x = currentPositionX;
        this.currentPlayerPosition.y = currenPositionY;
    }

    public void setDirection(Direction direction, float delta) {
        this.previousDirection = this.currentDirection;
        this.currentDirection = direction;

        // Look into the appropriate variable when changing position
        switch (currentDirection) {

            case DOWN:
                currentFrame = (TextureRegion) walkDownAnimation.getKeyFrame(frameTime);
                break;
            case LEFT:
                currentFrame = (TextureRegion) walkLeftAnimation.getKeyFrame(frameTime);
                break;
            case RIGHT:
                currentFrame = (TextureRegion) walkRightAnimation.getKeyFrame(frameTime);
                break;
            case UP:
                currentFrame = (TextureRegion) walkUpAnimation.getKeyFrame(frameTime);
                break;
            default: 
                break;
        }
    }

    public void setNextPositionToCurrent() {
        setCurrentPosition(nextPlayerPosition.x, nextPlayerPosition.y);
    }

    /**
     * This method is called every time the player input is detected.
     * It is used to dal with collisions between two moving objects
     * in the game world. We look ahead and predict what the next
     * position value will be by using our current velocity vector.
     * @param currentDirection
     * @param delta
     */
    public void calculateNextPosition(Direction currentDirection, float delta) {
        float testX = currentPlayerPosition.x;
        float testY = currentPlayerPosition.y;

        // Delta time scalar quantity. Get the value
        // that represents the distance we would travel.
        velocity.scl(delta);

        switch (currentDirection) {
            case LEFT:
                testX -= velocity.x;
                break;
            case RIGHT:
                testX += velocity.x;
                break;
            case UP:
                testY += velocity.y;
                break;
            case DOWN:
                testY -= velocity.y;
                break;
            default:
                break;
        }

        nextPlayerPosition.x = testX;
        nextPlayerPosition.y = testY;

        // Velocity
        velocity.scl(1 / delta);
    }

}
