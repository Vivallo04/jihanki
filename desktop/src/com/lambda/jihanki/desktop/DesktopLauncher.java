package com.lambda.jihanki.desktop;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.lambda.jihanki.Jihanki;

public class DesktopLauncher {

	private static final String TAG = DesktopLauncher.class.getSimpleName();

	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Jihanki";
		config.useGL30 = false;
		config.width = 1280;
		config.height = 720;
		config.forceExit = false;

		Gdx.app = new LwjglApplication(new Jihanki(), config);
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		Gdx.app.debug(TAG, "Game Initialized successfully!");
	}
}
